require 'net/http'
require "uri"
require 'nokogiri'
require 'date'
require 'csv'

def count_yes_no(kod)
    url_post_mregistr = 'http://w1.c1.rada.gov.ua/pls/radan_gs09/ns_dep_reg_w_list'
    params = {
        :startDate => "29.08.2019",
        :endDate => Date.today.strftime("%d.%m.%Y"),
        :kod => kod,
        :nom_str => "0",
    }
    uri_mregistr = URI(url_post_mregistr)
    res_mregistr = Net::HTTP.post_form(uri_mregistr, params)
    page_mregistr = Nokogiri::HTML(res_mregistr.body.force_encoding('WINDOWS-1251').encode("UTF-8"))
    reg_divs = page_mregistr.css("div[class='zrez']")
    registration = Array[]
    for a in reg_divs
        registration.push(a.text.strip)
    end
    group_mregistr = registration.group_by(&:itself).map { |k,v| [k, v.length] }.to_h
    yes = group_mregistr.inject({}){|option, (k,v) | option["yes"] = v if k.include? "За"; option}
    no = group_mregistr.inject({}){|option, (k,v) | option["no"] = v if k.include? "Не"; option}
    if no.length == 0
        no['no']=0
    end
    return yes.merge(no)
end

uri = URI('http://w1.c1.rada.gov.ua/pls/site2/fetch_mps')
params = {:skl_id => 10}
uri.query = URI.encode_www_form(params)
res = Net::HTTP.get_response(uri)
page = Nokogiri::HTML(res.body.force_encoding('WINDOWS-1251').encode("UTF-8"))
list_li = page.css('li')

deputy_names = Array[]
for obj in list_li
    href = obj.css('a')[0]['href']
    name = obj.css('a')[0].text
    dict = Hash.new
    dict['name'] = name
    dict['href'] = href
    deputy_names.push(dict)
end

full_dict_list = Array[]
deputy_names.each_with_index do |a,i|
    puts "index #{i} of #{deputy_names.length}"
    res_1 = Net::HTTP.get_response(URI(a['href']))
    page_1 = Nokogiri::HTML(res_1.body)
    kod_href = page_1.xpath('//*[@id="mp_content"]/div/div[contains(@class, "topTitle")]/a[3]')[0]['href']
    kod = kod_href.split("kod=")[1]
    a.merge(count_yes_no(kod))
    full_dict_list.push(a.merge(count_yes_no(kod)))
end

data = full_dict_list.sort_by { |k| k["no"] }.reverse!
file_name = 'deputy_manual_registration_' + Date.today.strftime("%d_%m_%Y") + '.csv'
CSV.open(file_name, "wb") do |csv|
    csv << data.first.keys # adds the attributes name on the first line
    data.each do |hash|
      csv << hash.values
    end
  end