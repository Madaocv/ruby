class ContactController < ApplicationController
  def index
  end

  def create
    @article = Contactpost.create(contact_params)
    flash[:success] = "Thank you for you message"
    # render :index
    redirect_to root_path
  end

  private
    def contact_params
      params.require(:article).permit(:message, :name, :email)
    end
end
