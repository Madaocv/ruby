class CreateContactposts < ActiveRecord::Migration[6.0]
  def change
    create_table :contactposts do |t|
      t.text :message
      t.string :name
      t.string :email

      t.timestamps
    end
  end
end
